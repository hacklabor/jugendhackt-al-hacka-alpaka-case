# Case for the Al Hacka Alpaka solder kit by blinkyparts.com

![Example case](./Case%20example.jpg)

Made at [Hacklabor Schwerin](https://hacklabor.de)

`alpaka_outline_only_line.svg` and `alpaka_outline_only_line.dxf` are edits of the original design files of the PCB.

Masterfile: `Alhacka Case.f3d` Fusion 360

## License: CC BY-SA 4.0

Alhacka Case © 2024 by Axel is licensed under CC BY-SA 4.0.

See [license file](license.CC-BY-SA-4.0.txt) for full text.
